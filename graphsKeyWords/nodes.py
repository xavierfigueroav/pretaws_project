

f=open('scopus_topics.csv', encoding='utf_8')
#Title	Link	Authors	Year	Source	Citations	Author1	Author2	Author3	Author4	Author5	Abstract	Keywords	Topics

#diccionario de forma {id:[nombre,listaDeKeyWords]}
dic={}
f.readline()
for line in f:
    L=line.strip().split('\t')
    autoresL = L[2].replace('(...), ', '').split('., ')
    idAutoresL =[]
    keyWordsL=[]

    linksAutores=L[6:11]

    for link in linksAutores:

        if link.find('Id=')!=-1:
            #print(link)
            id = link.split('Id=')[1][:-6]
            idAutoresL.append(id)

    for kw in L[-2].split(','):
        keyWordsL.append(kw)

    #print(autoresL)
    #print(idAutoresL)
    #print(keyWordsL)

    for i,id in enumerate(idAutoresL):

        if id not in dic.keys() and i<min(len(autoresL),len(idAutoresL)):

            dic[id]={'Nombre':autoresL[i],'KeyWords':[]}

    for id in idAutoresL:
        if id in dic.keys():
            dic[id]['KeyWords'].extend(keyWordsL)
print(dic)
print()

h= open('nodos.csv','w',encoding="utf_8")
h.write('ID;LABEL\n')

def ordenarLista(Lista):
    ListaOrdenada=[]

    topicMax=''
    while len(Lista)>0:
        max = 0
        for elem in Lista:
            if Lista.count(elem)>max and elem not in ListaOrdenada:
                max = Lista.count(elem)
                topicMax=elem
        ListaOrdenada.append(elem)
        for i in range(Lista.count(elem)):
            Lista.remove(elem)


    return ListaOrdenada


for key,value in dic.items():
    Lista=ordenarLista(value["KeyWords"])
    linea=value["Nombre"]+";"+'/'.join(Lista[:min(len(Lista)-1,7)])+"\n"
    print(linea)
    h.write(linea)



L=["HOLA","juan","ro", "HOLA", "HOLA"]

print(ordenarLista(L))
