import numpy as np

#Recibe como parametros:
#filename --> nombre de archivo a leer
#yearsL--> lista de anios(int) en los que se desea cargar informacion,
#          si no se coloca este parametro se cargan todos los anios
#Devuelve:
#Tupla= (VerticesL,ListaAdyacencia)
#   VerticesL: lista de todos los id sin repetir
#   ListaAdyacencia= lista de listas de las relaciones de los id de autores en cada paper
def cargarDatos(filename, yearsL=range(2020)):
    f = open(filename, encoding='utf-8')
    VerticesL = []
    ListaAdyacencia=[]
    f.readline()
    for line in f:
        lineL = line.strip().split(";")
        year=lineL[3]
        lineL = lineL[6:11]
        Autores=[]

        if year=="":
            year=1
        else:
            year=int(year)
        for link in lineL:
            if link != "" and year in yearsL:
                idAutor = link.split('Id=')[1][:-6]
                Autores.append(idAutor)
                if idAutor not in VerticesL:
                    VerticesL.append(idAutor)
        if len(Autores)>0:
            ListaAdyacencia.append(Autores)
    return VerticesL,ListaAdyacencia

#Creo una Matriz de adyacencia
#Recibe: VerticesL y Lista de Adyacencia sacados de la funcion anterior
#Retorna una matriz de numpy
def crearMatriz(VerticesL, ListaAdyacencia):
    n=len(VerticesL)
    Matriz= np.zeros((n,n), dtype='int')

    for Autores in ListaAdyacencia:
        for autor1 in Autores:
            for autor2 in Autores:
                id1=VerticesL.index(autor1)
                id2=VerticesL.index(autor2)
                if id1!=id2:
                    Matriz[id1][id2]+=1
                    Matriz[id2][id1]+=1

    return Matriz

#Escribe un arcivo csv
#recibe como parametros:
#filename: nombre que tendra el archivo
#matriz: matriz de numpy de la funcion anterior
#VerticesL: lista de los id
def escribirArchivoCSV(filename, matriz, VerticesL):
    h=open(filename,'w',encoding='utf-8' )
    h.write(","+",".join(VerticesL)+'\n')
    for i, fila in enumerate(matriz):
        lista=[]
        for num in fila:
            lista.append(str(num))
        linea=str(VerticesL[i]) + ','+ ",".join(lista) + '\n'
        h.write(linea)


def main():
    VerticesL , ListaAdyacencia= cargarDatos("scopus_cleaned.csv", list(range(2016,2019)))
    #2001,2002,2003,2004,2005

    matriz=crearMatriz(VerticesL, ListaAdyacencia)
    escribirArchivoCSV("Data2016-2018.csv", matriz, VerticesL)

main()

