from selenium import webdriver
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.firefox.options import Option

options = Options()
options.headless = True
browser = webdriver.Firefox(options=options)

new_scopus_file = open("scopus_topics.csv", 'w', encoding='utf-8')
new_scopus_file.write("Title\tLink\tAuthors\tYear\tSource\tCitations\tAuthor1\tAuthor2\tAuthor3\tAuthor4\tAuthor5\tAbstract\tKeywords\n")

# Title;Link;Authors;Year;Source;Citations;Author1;Author2;Author3;Author4;Author5;Abstract,Keywords
with open('scopus/scopus_cleaned.csv', 'r', encoding='utf-8') as file:
    file.readline()
    for line in file:
        line = line.strip()
        fields = line.split(";")

        try:
            browser.get(fields[1])
            select_element = WebDriverWait(browser, 10).until(EC.visibility_of_element_located((By.XPATH, "//a[@title='Open Topic details']")))
        except:
            print("Error on link: " + fields[1])
            continue

        topics = ",".join(select_element.text.split(' | '))
        abstract = ";".join(fields[11:-1])

        written = '\t'.join(fields[:11]) + '\t' + abstract + '\t' + fields[-1] + '\t' + topics + '\n'
        new_scopus_file.write(written)
    browser.quit()

new_scopus_file.close()