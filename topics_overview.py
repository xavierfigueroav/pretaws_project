topics_list = []
uniques_topics = set()
uniques_lists = set()
with open('scopus/scopus_topics.csv', 'r', encoding='utf-8') as file:
    for line in file:
        line = line.strip()
        fields = line.split('\t')
        topics = fields[-1]
        topics_list.append(topics)
        uniques_topics.update(topics)
        uniques_lists.add(topics)
        print("Topics: " + str(topics.split(',')))
print("Total topics: %d" % (3 * len(topics_list)))
print("Unique topics: %d" % len(uniques_topics))
print("Unique group of topics: %d" % len(uniques_lists))