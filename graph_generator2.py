# Title;Link;Authors;Year;Source;Citations;Author1;Author2;Author3;Author4;Author5
from networkx import Graph, write_gexf
graph = Graph()

def graphGenerator(start=1994, end=2019):
    id_names = dict()
    with open('scopus/scopus_cleaned.csv', 'r', encoding='utf-8') as file:
        file.readline()

        for index, line in enumerate(file):
            line = line.strip()
            fields = line.split(';')[:11]
            print(fields)
            authorsNames = fields[2].replace('(...), ', '').split('., ')
            authorsNames[-1] = authorsNames[-1][:-1] if authorsNames[-1].endswith('.') else authorsNames[-1]

            citations = int(fields[5])
            authorsLinks = fields[6:]
            year = fields[3]
            if year!='' and int(year)in range(start, end+1):
                for i in range(len(authorsNames)):
                    for j in range(i + 1, len(authorsNames)):

                        linkAuthor1 = authorsLinks[i]
                        linkAuthor2 = authorsLinks[j]

                        if linkAuthor1 != '' and linkAuthor2 != '':

                            author1ID = linkAuthor1.split('Id=')[1][:-6]
                            author2ID = linkAuthor2.split('Id=')[1][:-6]

                            author1name = authorsNames[i]
                            author2name = authorsNames[j]

                            if author1ID not in id_names:
                                id_names[author1ID] = author1name
                                graph.add_node(author1name)

                            if author2ID not in id_names:
                                id_names[author2ID] = author2name
                                graph.add_node(author2name)

                            if id_names[author2ID] not in graph[id_names[author1ID]]:
                                graph.add_edge(id_names[author1ID], id_names[author2ID], citations=citations, weight=1)
                            else:
                                graph[id_names[author1ID]][id_names[author2ID]]['citations'] += citations
                                graph[id_names[author1ID]][id_names[author2ID]]['weight'] += 1


graphGenerator(1994, 2015)
graphGenerator(1994, 2018)

